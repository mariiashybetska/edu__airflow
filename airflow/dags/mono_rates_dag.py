from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.bash import BashOperator
from airflow.utils.dates import days_ago

from utils import get_mono_rates

default_args = {
    'owner': 'mariia',
}

dag = DAG(
    'mono',
    default_args=default_args,
    description='Get monobank rate for USD and EUR',
    schedule_interval='*/5 * * * *',
    start_date=days_ago(1),
    tags=['hillel'],
)


task1 = PythonOperator(
    task_id='get_mono_rates',
    dag=dag,
    python_callable=get_mono_rates,
)

task2 = BashOperator(
    task_id='bash',
    dag=dag,
    bash_command='echo end',
)

task1 >> task2


