import clickhouse_driver
import requests

from decimal import Decimal

DATABASE_URL='your db url'

ch_cl = clickhouse_driver.Client.from_url(DATABASE_URL)

def get_mono_rates():
    url = 'https://api.monobank.ua/bank/currency'
    response = requests.get(url)
    r_json = response.json()

    for rate in r_json:
        try:
            if rate['currencyCodeA'] in {840, 978} and rate['currencyCodeB'] == 980:
                print(f"currency: {rate['currencyCodeA']}, buy: {round(Decimal(rate['rateBuy']), 2)}, sale': {round(Decimal(rate['rateSell']), 2)}")

                ch_cl.execute(f'''
                insert into table_name (CreatedAt, CurrencyCode, BuyRate, SaleRate, UpdtedAt)
                values (today(), {rate['currencyCodeA']}, {round(Decimal(rate['rateBuy']), 2)}, {round(Decimal(rate['rateSell']), 2)}, now())
                ''', types_check=True)

                ch_cl.execute('optimize table table_name final')

        except (TypeError):
            raise TypeError('Too many requests!')
